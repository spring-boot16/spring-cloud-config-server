package com.dl.microservices.springcloudconfigserver;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Disabled
class SpringCloudConfigServerApplicationTests {

    @Test
    void contextLoads() {
    }

}
